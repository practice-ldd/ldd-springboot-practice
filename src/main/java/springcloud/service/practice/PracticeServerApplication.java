package springcloud.service.practice;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = {"com.demo2do","com.suidifu", "com.zufangbao"})
public class PracticeServerApplication {

    public static void main(String[] args) {

        SpringApplication.run(PracticeServerApplication.class, args);

    }
}
