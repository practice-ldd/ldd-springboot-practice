package springcloud.service.practice.test;

import com.alibaba.fastjson.JSON;
import com.suidifu.swift.notifyserver.notifyserver.NotifyApplication;
import com.suidifu.swift.notifyserver.notifyserver.NotifyJob;
import com.suidifu.swift.notifyserver.notifyserver.PostEntityType;
import com.suidifu.swift.utils.HttpClientUtil;
import com.zufangbao.sun.utils.JsonUtils;
import com.zufangbao.sun.utils.StringUtils;
import com.zufangbao.sun.utils.uuid.UUIDUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;

public class DigitalSignatureMain {

    /**
     * 秘钥验证
     * @param args
     * @throws FileNotFoundException
     */
//    public static void main(String[] args) throws Exception {
//        String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALBXbt66OMAW4a5RQDT7DUj5OXd90XBbqFziQSpoVDQ6wlvt/4R/hwJUcnO2jHCF5TtPVawDFaHt/2eJeSpNHNrrvfHIbeiYMUVPHLa6Zz0STJVGJydJDvgLiS92lwGWNx4gtdiNGOPPwEtriCwt3FIwklMUD0q3FOSaYygmNGhlAgMBAAECgYAHTXef7aJQAiKSb3bzOrpQHVIN6r3zozgTsKL2OJ/UEeLen7qHKCjVJd4fgJ3MQx89F4n6t537uLls9jp4T/0z2g2gjsxT5HMlLxY3GwPuCB8Lhftby7n0A8vrL06jo86RpdmbLqbIIE0ozJv0CtwxQbgzbp1gIYR0yd0hY1JeeQJBAOUN0ffXxE1E93HCPXBc5i4/2DXykYeXIW+KdenmTmTZp7CYtjCUWcS9EdhXyDm6hysDwpmahqDxTY/gvVYUbMcCQQDFFiCHfq8RGQyRwsem8qjCvtvnwDVApJK36/SrpUqjSnXVHNkj7PqQ1XepwZcwqr4zo5WvA4E4OsPo7+fYox1zAkBnpvZiWqC5esEjGIwnAdMkEL4tw4Q5wJlf2V98pjzJhHlO//xKRYfN8OVXannwLcmnHH9+dSABStEXj9L+ViXXAkEAldYhuwF8VZJmtPaR1YIM/aofA81kbxXvmbbhGBIdcZGtje1VxHNpd+1jAYN1z4EwA5Btv1X5yucahKDH8jLoswJAW1ZVgn8ExSI/4LH77uGDMFsTdqIN2JGv8gZaN40t5hRlfJSb0v3V5exBsZXOZGxNqJLrvAnkK0TlikxT1UK+BQ==";
//        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCwV27eujjAFuGuUUA0+w1I+Tl3fdFwW6hc4kEqaFQ0OsJb7f+Ef4cCVHJztoxwheU7T1WsAxWh7f9niXkqTRza673xyG3omDFFTxy2umc9EkyVRicnSQ74C4kvdpcBljceILXYjRjjz8BLa4gsLdxSMJJTFA9KtxTkmmMoJjRoZQIDAQAB";
//        String content = JsonUtils.toJSONString("123456");
//        HashMap<String, String> newMap = new HashMap<>();
//        newMap.put("message", content);
//
//
//        String md5Sign = rsaSign(content, privateKey);
//        System.out.println("sign =" + md5Sign);
//
//        boolean result = rsaCheckContent(content,md5Sign,publicKey);
//        System.out.println(result);
//    }

    /**
     * 文件md5值获取
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {


        String md5 = getMD5Three("/home/liudong/桌面/file/test.txt");
        System.out.println(md5);
        String messageDigest = null;
        try {
            messageDigest = DigestUtils.md5Hex(new FileInputStream("/home/liudong/桌面/file/test.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(messageDigest);
    }

    public static String getMD5Three(String path) {
        BigInteger bi = null;
        try {
            byte[] buffer = new byte[8192];
            int len = 0;
            MessageDigest md = MessageDigest.getInstance("MD5");
            File f = new File(path);
            FileInputStream fis = new FileInputStream(f);
            while ((len = fis.read(buffer)) != -1) {
                md.update(buffer, 0, len);
            }
            fis.close();
            byte[] b = md.digest();
            bi = new BigInteger(1, b);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bi.toString(16);
    }



    /**
     * MD5_WITH_RSA数字签名
     *
     * @param content
     * @param sign
     * @param
     * @return
     */
    public static boolean rsaCheckContent(String content, String sign, String publicKey) {
        try {
            if (StringUtils.isEmpty(sign) || StringUtils.isEmpty(publicKey)) {
                System.out.println("验签失败，sign签名为空，或者商户未上传公钥！");
                return false;
            }
            PublicKey pubKey = getPublicKey(publicKey);
            Signature signature = Signature.getInstance("MD5withRSA");
            signature.initVerify(pubKey);
            signature.update(content.getBytes());
            return signature.verify(Base64.getDecoder().decode(sign.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 获取公钥
     *
     * @param
     * @return
     */
    private static PublicKey getPublicKey(String publicKey) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] encodedKey = Base64.getDecoder().decode(publicKey);
            PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));
            return pubKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * RSA 签名
     *
     * @param content    签名内容
     * @param privateKey 私钥
     * @return
     */
    public static String rsaSign(String content, String privateKey) {
        try {
            PrivateKey priKey = getPrivateKeyFromPKCS8("RSA", privateKey);

            Signature signature = Signature.getInstance("MD5withRSA");

            signature.initSign(priKey);

            signature.update(content.getBytes());

            byte[] signed = signature.sign();

            return new String(java.util.Base64.getEncoder().encode(signed));
        } catch (InvalidKeySpecException e) {
//            logger.error("RSA私钥格式不正确，请检查是否正确配置了PKCS8格式的私钥！", e);
            System.out.println("RSA私钥格式不正确，请检查是否正确配置了PKCS8格式的私钥！" + e.getMessage());
        } catch (Exception e) {
//            logger.error("签名错误！", e);
            System.out.println("签名错误！" + e.getMessage());
        }
        return null;
    }

    /**
     * 获取PKCS8格式私钥
     *
     * @param algorithm  RSA
     * @param privateKey 私钥
     * @return
     * @throws Exception
     */
    public static PrivateKey getPrivateKeyFromPKCS8(String algorithm, String privateKey) throws Exception {
        PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(java.util.Base64.getDecoder().decode(privateKey));
        KeyFactory keyf = KeyFactory.getInstance(algorithm);
        PrivateKey priKey = keyf.generatePrivate(priPKCS8);
        return priKey;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class FailedContractInfo {

        String uniqueId;//贷款合同唯一识别号
        String contractNo;//贷款合同编号
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class FailedContractInfoParam {

        String requestId;
        String requestNo;
        String requestIp;
        String requestTime;
        String completeTime;
        String contractDetailSize;
        String financialProductCode;
        String contractsTotalNumber;
        String contractsTotalAmount;
        String successCount;
        String loanBatchId;
        String failedCount;
        List<FailedContractInfo> failedContractInfoString;

    }

}