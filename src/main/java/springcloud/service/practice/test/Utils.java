package springcloud.service.practice.test;

import com.demo2do.core.entity.Result;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.util.StreamUtils;

public class Utils {

    public static Result executePostRequest(String url, Map<String, String> params, Map<String, String> headerParams) throws Exception {

        List<BasicNameValuePair> list = new ArrayList();
//            if (params != null) {
//                Iterator var4 = params.entrySet().iterator();
//
//                while(var4.hasNext()) {
//                    Entry<String, String> entry = (Entry)var4.next();
//                    formParams.add(new BasicNameValuePair((String)entry.getKey(), (String)entry.getValue()));
//                }
//            }
        params.forEach((k, v) -> {
            list.add(new BasicNameValuePair(k, v));
        });

//            return executePostRequest(url, (HttpEntity)(new UrlEncodedFormEntity(list, "UTF-8")), headerParams);


        HttpEntity reqEntity = (HttpEntity) new UrlEncodedFormEntity(list, "UTF-8");


        HttpPost httpPost = new HttpPost(url);
        Result result = new Result();

        Result var6;

        httpPost.setEntity(reqEntity);
        if (headerParams != null) {
            Iterator var5 = headerParams.entrySet().iterator();

            while (var5.hasNext()) {
                Entry<String, String> entry = (Entry) var5.next();
                httpPost.addHeader((String) entry.getKey(), (String) entry.getValue());
            }
        }

        CloseableHttpClient httpclient = HttpClients.createDefault();
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectTimeout(60000).setConnectionRequestTimeout(60000).build();
        httpPost.setConfig(requestConfig);
        HttpResponse httpResp = httpclient.execute(httpPost);
        int statusCode = httpResp.getStatusLine().getStatusCode();
        HttpEntity rspEntity = httpResp.getEntity();
        InputStream in = rspEntity.getContent();
        String strResp = StreamUtils.copyToString(in, Charset.forName("UTF-8"));
        result.data("responsePacket", strResp);
        result.data("responseHttpStatus", "" + statusCode);
        Result var12;
        if (statusCode != 200 && statusCode != 502 && statusCode != 504) {
            var12 = result.message("http响应失败！(" + statusCode + ")");
            return var12;
        }

        var12 = result.success();
        return var12;
    }
}
