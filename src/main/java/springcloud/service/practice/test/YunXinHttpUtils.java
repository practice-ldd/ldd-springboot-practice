package springcloud.service.practice.test;

import com.alibaba.fastjson.JSONObject;
import com.demo2do.core.utils.StringUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;

/**
 *
 * @author WJX
 * @date 2018/8/2 20:06
 * @description 请求http工具类
 */
public class YunXinHttpUtils {
    /**
     * 请求成功码
     */
    private static final Integer SUCCESS_CODE = 200;

    /**
     * 发送http请求
     * @param workParam 报文
     * @return
     */
    public static String sendHttpRequest(Map<String, String> workParam) {
        try {
            String head = workParam.getOrDefault("head", StringUtils.EMPTY);
            String body = workParam.getOrDefault("body", StringUtils.EMPTY);

            Map<String, String> headData = JSONObject.parseObject(head, Map.class);

            URL url = new URL(headData.getOrDefault("url", StringUtils.EMPTY));

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            //发送POST请求
            conn.setRequestMethod("POST");
            //设置超时时间
            conn.setConnectTimeout(1000000);
            conn.setReadTimeout(10000000);

            conn.setRequestProperty("Content-Type","application/json;charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");

            conn.setRequestProperty("merid", headData.getOrDefault("merid", StringUtils.EMPTY));
            conn.setRequestProperty("secretkey", headData.getOrDefault("secretkey", StringUtils.EMPTY));
            conn.setRequestProperty("signedmsg", headData.getOrDefault("signedmsg", StringUtils.EMPTY));

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes());
            os.flush();

            if (conn.getResponseCode() != SUCCESS_CODE) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
            StringBuffer stringBuffer = new StringBuffer();

            while ((output = br.readLine()) != null) {
                System.out.println(output);
                stringBuffer.append(output);
            }

            conn.disconnect();
            return stringBuffer.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return StringUtils.EMPTY;
    }
}