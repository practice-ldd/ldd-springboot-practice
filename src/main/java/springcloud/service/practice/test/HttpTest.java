package springcloud.service.practice.test;

import com.demo2do.core.entity.Result;
import com.zufangbao.gluon.opensdk.HttpClientUtils;
import com.zufangbao.sun.utils.JsonUtils;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.HashMap;

/**
 * @Auther: liudong
 * @Date: 18-9-20 16:35
 * @Description:
 */
public class HttpTest {
    public static void main(String[] args){
        String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOXUGOKdEkssOI1zqNw4pb6emH1o1JYxooyTQ7FN1IBNqTJLuvA3GsswXIkuQj0imce6ywG/XOCwc9R1l5FwcORtwx2FihGCl7eBkhUwnT0EwGOEARPh96ey+TfvsvRaHOn672v1TEhajAftgm4l7fJDtHdGBjHOs+5Mlir9Z65RAgMBAAECgYEArtAiUZR5yrYLGgTEhyWLZK+Le7CWKtv8MQL+tUlm/mST8s7JlVfEyJKzgCCwf4HnCJXbPiwJgFqW8B61uAmXw6cEoPftEnzvKBTyISt/iEf7DTWKGkDBnlYM9sFU6pU61jw17XEDQRtSBG6cfrlGSelqf25+c8onxu4YwTeLH/ECQQD/H69tPy0FYRvCJ5yXdXEVCKshNN01P+UdDzGtyysE/gmpalbewT+uznApa0qFntcYb8eSpUJzrUlItSCBGUpdAkEA5p4r3qF+4g5V7MBHm3+v1l9JKxYK76990AQJa122rfkY2EEVuvU+8KIAQpVflu/HpDe8QH4mQZTsZj24Skt8hQJAL5j2vrgRqzZB2ohPY8aKcXUrkEdvmdaw5SoHh7gm74iBvvTS/j4ppnBnZqLYxXMsCCgaoNZqPnCvAnygctWIFQJAHm2KLkKyohLwJV+tUwgC5E8IMWYkJUHLYNHXiFICE2xFaesUeel313oYfLCGvzx9493yubOrSoXitw63rR3OnQJBALwGSnGYodmJB5k7un0X6LlO4nSu/+SX9lweloZ1AUg15IuCIYxHAFKwOtOJmx/eMcITaLq8l1qzZ907UXY+Mfs=";
        HashMap<String, Object> requestParameters = getContent();
        String content = JsonUtils.toJSONString(requestParameters);
        HashMap<String, String> header = getHeader(content,privateKey);


        Result result = HttpClientUtils.executePostRequest("http://101.52.128.166/Loan/ImprotPkgCallBack", JsonUtils.toJSONString(requestParameters), header);
        System.out.println(result);
    }

    private static HashMap<String,String> getHeader(String content, String privateKey) {
        HashMap<String,String> header = new HashMap<>();
        String md5Sign = rsaSign(content, privateKey);
        header.put("merid", "suidifu");
        header.put("secretkey", "suidifutest");
        header.put("Content-Type", "application/json");
        header.put("signedmsg", md5Sign);
        return header;
    }

    private static HashMap<String,Object> getContent() {
        return new HashMap<>();
    }

    /**
     * RSA 签名
     *
     * @param content    签名内容
     * @param privateKey 私钥
     * @return
     */
    public static String rsaSign(String content, String privateKey) {
        try {
            PrivateKey priKey = getPrivateKeyFromPKCS8("RSA", privateKey);

            Signature signature = Signature.getInstance("MD5withRSA");

            signature.initSign(priKey);

            signature.update(content.getBytes());

            byte[] signed = signature.sign();

            return new String(java.util.Base64.getEncoder().encode(signed));
        } catch (InvalidKeySpecException e) {
//            logger.error("RSA私钥格式不正确，请检查是否正确配置了PKCS8格式的私钥！", e);
            System.out.println("RSA私钥格式不正确，请检查是否正确配置了PKCS8格式的私钥！" + e.getMessage());
        } catch (Exception e) {
//            logger.error("签名错误！", e);
            System.out.println("签名错误！" + e.getMessage());
        }
        return null;
    }

    /**
     * 获取PKCS8格式私钥
     *
     * @param algorithm  RSA
     * @param privateKey 私钥
     * @return
     * @throws Exception
     */
    public static PrivateKey getPrivateKeyFromPKCS8(String algorithm, String privateKey) throws Exception {
        PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(java.util.Base64.getDecoder().decode(privateKey));
        KeyFactory keyf = KeyFactory.getInstance(algorithm);
        PrivateKey priKey = keyf.generatePrivate(priPKCS8);
        return priKey;
    }
}
