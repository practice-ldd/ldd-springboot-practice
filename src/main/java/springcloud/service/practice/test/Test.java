package springcloud.service.practice.test;


import static com.zufangbao.sun.utils.FilenameUtils.SIGNAL_FILE_SPLIT;
import static springcloud.service.practice.test.ResultFileSign.BUCKUP;
import static springcloud.service.practice.test.ResultFileSign.FILE_TYPE_DAILY_ASSET_CHANGE_RECORD;
import static springcloud.service.practice.test.ResultFileSign.FILE_TYPE_DAILY_ASSET_INCREMENT_RECORD;
import static springcloud.service.practice.test.ResultFileSign.FILE_TYPE_REMITTANCE_RECORD;
import static springcloud.service.practice.test.ResultFileSign.FILE_TYPE_REPAYMENT_RECORD_OFFLINE;
import static springcloud.service.practice.test.ResultFileSign.FILE_TYPE_REPAYMENT_RECORD_ONLINE;
import static springcloud.service.practice.test.ResultFileSign.FILE_TYPE_REPURCHASE_RECORD;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import com.zufangbao.gluon.api.earth.v3.model.ApiMessage;
import com.zufangbao.gluon.util.ApiSignUtils;
import com.zufangbao.sun.entity.sftp.SftpConfig;
import com.zufangbao.sun.entity.sftp.SftpInfoModel;
import com.zufangbao.sun.utils.DateUtils;
import com.zufangbao.sun.utils.FileUtils;
import com.zufangbao.sun.utils.FilenameUtils;
import com.zufangbao.sun.utils.StringUtils;
import com.zufangbao.sun.utils.excel.ExcelUtil;
import com.zufangbao.sun.yunxin.entity.files.FileSignal;
import com.zufangbao.sun.yunxin.entity.files.FileTmp_MutableFee;
import com.zufangbao.sun.yunxin.entity.files.FileTmp_OverdueFee;
import com.zufangbao.sun.yunxin.handler.SftpHandler;
import com.zufangbao.sun.yunxin.service.DictionaryService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: liudong
 * @Date: 18-9-7 16:42
 * @Description:
 */
@RestController
public class Test {

    @Resource
    private DictionaryService dictionaryService;

    @Resource
    private SftpHandler sftpHandler;
    public static void main(String[] args) throws IOException {
        Test test = new Test();
        test.lexindailyAssetIncrementResultFileUpload();
    }

    @org.junit.Test
    public void makeTest(){
        Test test = new Test();
        test.lexindailyAssetIncrementResultFileUpload();
    }


    /**
     * 日中资产,还款记录文件上传(定时任务)
     */
    public void lexindailyAssetIncrementResultFileUpload(){
        SftpInfoModel sftpInfoModel = getSftpInfo();
        if (null == sftpInfoModel){
//            LOGGER.error("asyncRepaymentOrderResultFile responseFile 无对应的文件存储信息");
            return;
        }
        List<String> zangxinList = new ArrayList<>();
        sftpInfoModel.setDstPath(sftpInfoModel.getDstPath()+ new SimpleDateFormat("yyyyMMdd").format(DateUtils.getToday())+File.separator);
        zangxinList.add("/home/liudong/桌面/file/productCode1_20181130_dailyAssetIncrementRecord.txt");
        upLoad(sftpInfoModel,zangxinList);

    }

    private SftpInfoModel getSftpInfo() {
        SftpInfoModel sftpInfoModel = new SftpInfoModel();
        sftpInfoModel.setDstPath("/home/liudong/桌面/file/zangxin/");
        sftpInfoModel.setUserName("lexin");
        sftpInfoModel.setPassWord("2ac09bc6");
        sftpInfoModel.setPort("2022");
        sftpInfoModel.setHost("120.26.102.180");
        sftpInfoModel.setSrcPath("/home/liudong/桌面/file/");
        return sftpInfoModel;
    }

    /**
     * 日中资产,还款记录等6份文件上传
     * @param sftpInfoModels
     * @param zangxinList
     */
    private void getResponseFileData(List<SftpInfoModel> sftpInfoModels, List<String> zangxinList,List<String> typeList) {
        //TODO:本地存储文件路径待定
        String localPath = sftpInfoModels.get(0).getSrcPath();
        if (!new File(localPath).isDirectory()){
            return;
        }
        Iterator itFile = FileUtils.iterateFiles(new File(localPath), null, false);//信号文件
        if (!itFile.hasNext()){
//            LOGGER.info("FileProcessTask getResponseData itFile is null 无回调文件:"+localPath);
        }
        while (itFile.hasNext()) {
            File signalResponseFile = (File) itFile.next();
            String signalResponseName = signalResponseFile.getName();
//            LOGGER.info("asyncRepaymentOrderResultFile, signalResponseName:" + signalResponseName);
            //TODO:文件命名格式待确认
            String temp = signalResponseName.split(FilenameUtils.UNDERLINE_SPLIT)[2];
            if (!temp.contains(".")){
                temp = temp + ".";
            }
            String businessType = temp.substring(0,temp.indexOf(".")).trim();//文件类型
            String createDate = signalResponseName.split(FilenameUtils.UNDERLINE_SPLIT)[1];//文件生成时间

            if (typeList.contains(businessType) && new SimpleDateFormat("yyyyMMdd").format(DateUtils.getToday()).equals(createDate)){
                zangxinList.add(signalResponseFile.getAbsolutePath());
            }
            continue;
        }
    }
    /**
     * 上传sftp
     * @param sftpInfoModel sftp配置信息
     * @param list 上传的文件路径集合
     */
    private void upLoad(SftpInfoModel sftpInfoModel, List<String> list) {
            for (String path : list) {
                File reconciliationFile = new File(path);
                ChannelSftp chSftp = sftpHandler.getChannelSftpBySftpInfoModel(sftpInfoModel);
                if (null == chSftp) {
                    return;
                }

                String src = reconciliationFile.getAbsolutePath();
                String dst = sftpInfoModel.getDstPath() + reconciliationFile.getName();
                try {
//                    chSftp.put(src, dst);
                    File file = new File(reconciliationFile.getParent() + File.separator + com.zufangbao.sun.utils.ResultFileSign.BUCKUP + File.separator + reconciliationFile.getName());
                    try {
                        FileUtils.moveFile(new File(src), file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                sftpHandler.closeSFTPChannel(chSftp);
            }
    }

    /**
     * 获取文件名称
     *
     * @param repaymentOrderSignFile 信号文件路径
     * @return
     */
    private String[] getFileNames(File repaymentOrderSignFile) {
        String responsebusFileName = repaymentOrderSignFile.getName().replace(SIGNAL_FILE_SPLIT, StringUtils.EMPTY);
        String responseSignalFileName = responsebusFileName + FilenameUtils.SIGNAL_FILE_SPLIT;
        return new String[]{responsebusFileName, responseSignalFileName};
    }

}

@Data
@AllArgsConstructor
@NoArgsConstructor
class ResponseClass {
    private String completeTime = "";
    private int code = 0;
    private String comment = "";
    private String reqTradeNo = "";
}

class ResultFileSign {

    public static final String FENQILE = "fenqile";
    public static final String LEXIN = "lexin";

    //上传完成后备份文件夹
    public static final String BUCKUP = "backup";

    //6份对账标准文件
    public static final String FILE_TYPE_REMITTANCE_RECORD = "remittanceRecord";
    public static final String FILE_TYPE_REPAYMENT_RECORD_ONLINE = "repaymentRecordOnline";
    public static final String FILE_TYPE_REPAYMENT_RECORD_OFFLINE = "repaymentRecordOffline";
    public static final String FILE_TYPE_REPURCHASE_RECORD = "repurchaseRecord";
    public static final String FILE_TYPE_DAILY_ASSET_CHANGE_RECORD = "dailyAssetChangeRecord";
    public static final String FILE_TYPE_DAILY_ASSET_INCREMENT_RECORD = "dailyAssetIncrementRecord";
}