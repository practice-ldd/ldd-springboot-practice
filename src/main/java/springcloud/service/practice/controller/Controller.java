package springcloud.service.practice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zufangbao.gluon.api.earth.v3.model.ApiMessage;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import springcloud.service.practice.util.BaseResponse;

/**
 * Created by Raye on 2017/5/22.
 */
@RestController
public class Controller {

    @Resource
    private RestTemplate template;
    @RequestMapping("hello")
    public String ribbon(String name){
        return template.getForObject("http://SERVICE-HI/ribbonhello?name="+name,String.class);
    }

    @RequestMapping("feignhello")
    public String hello(String name){
        return "hello "+name+" this is ribbon spring cloud";
    }

    @GetMapping("nonNullTest")
    public String nonNullTest(){
        BaseResponse baseResponse = getResponse();
        String result = null;
        try {
            result = new ObjectMapper().writeValueAsString(baseResponse);
            System.out.println(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (null == baseResponse){
            System.out.println("baseResponse是空的");
        }else {
            System.out.println(baseResponse);
        }
        return "";
    }

    private static BaseResponse getResponse() {
        BaseResponse baseResponse = new BaseResponse();
        System.out.println(baseResponse+"fffffff");
        baseResponse.setCode(ApiMessage.SYSTEM_ERROR.getCode());
        return baseResponse;
    }
}