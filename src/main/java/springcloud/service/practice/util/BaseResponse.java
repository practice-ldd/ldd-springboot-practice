package springcloud.service.practice.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@ApiModel(value = "响应消息基础类")
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class BaseResponse {
    @NonNull
    @ApiModelProperty(value = "响应码", required = true)
    private int code;
    @NonNull
    @ApiModelProperty(value = "响应消息", required = true)
    private String message;
    @ApiModelProperty(value = "返回数据")
    private Object data;
}